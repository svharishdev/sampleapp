import { PermissionsAndroid } from 'react-native';

export async function requestLocationPermission() {
    let granted = await PermissionsAndroid.check(PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION);
    console.log("[Location Permission Given]",granted);

    try {
        const granted = await PermissionsAndroid.request(
            PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
            {
                'title': 'Cafe Sample App',
                'message': 'Cafe Sample App uses Location to Find near by Restuarants.'
            }
        )
        if (granted === PermissionsAndroid.RESULTS.GRANTED) {
            console.log("You can use the location")
        } else {
            console.log("location permission denied")
        }
    } catch (err) {
        console.warn(err);
    }
}