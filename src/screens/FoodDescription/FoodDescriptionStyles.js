import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
    scrollStyle: {
        flex: 1,
        marginBottom: 10
    },
    
    InfoPanel: {
        width: '100%'
    },
    InfoBasicPanel: {
        paddingLeft: 18
    },
    InfoTitle: {
        color: '#0080c0',
        fontWeight: '500',
        fontSize: 23 * 1.2,
        marginTop: 5
    },
    InfoHeader: {
        color: '#c0c0c0',
        fontWeight: '100',
        fontSize: 12 * 1.2,
        marginTop: 5
    },
    InfoAddress: {
        paddingTop: 10,
        paddingBottom: 14,
        borderBottomWidth: 0.6,
        borderBottomColor: '#c0c0c0',
        width: 180,
    },
    InfoAddressText: {
        textAlign: 'left',
        fontSize: 14 * 1.2
    },
    
    InfoLocationPanel: {
        height: 200,
        width: '100%',
        bottom: 0
    },
    InfoMarkerComponent: {
        width: 30,
        height: 30,
        backgroundColor: '#55a'
    },
    InfoReviewBtnPanel: {
        position: 'absolute',
        bottom: 0,
        width: '100%'
    },
    InfoReviewBtn: {
        width: '80%',
        backgroundColor: '#0076FF',
        height: 51,
        justifyContent: 'center',
        alignSelf: 'center',
        textAlignVertical: 'center',
        marginLeft: 20,
        marginRight: 30,
        borderRadius: 5
    },
    InfoReviewBtnText: {
        color: '#ffffff',
        fontWeight: '500',
        fontSize: 18 * 1.2,
        paddingLeft: 15,
        paddingRight: 15,
        textAlign: 'center'
    },
    
});

export default styles;