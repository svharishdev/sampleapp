import React from 'react';
import { View, Text, StyleSheet, Dimensions, Image, } from 'react-native';
export function HeaderTitle(props) {
    return (
        <View style={styles.foodDescHeaderTitlePanel}>
            <Text style={styles.foodDescHeaderTitle}>8.5</Text>
        </View>
    );
}
export function ImageCard(props) {
    return (
        <View style={styles.imageCard}>
            <Image source={{ uri: props.imageURL }}
                style={styles.imageStyle} />
            <View style={styles.CardTextualInfo}>
                <Text style={styles.CardTitle}>{props.title}</Text>
                <Text style={styles.CardDescription}>{props.description}</Text>
            </View>
        </View>
    );
}
export function InfoHeader(props) {
    return (
        <View><Text style={{ color: '#c0c0c0', fontWeight: '100', fontSize: 12 * 1.2, marginTop: 5 }}>{props.title}</Text></View>
    )
}

export function CategoryList(props) {

    return (
        <View style={styles.InfoCategoryList}>
            <Text style={styles.InfoCategoryListText}>
                {props.categories.toString()}
            </Text>
            <View style={styles.InfoEndHorizontalCrossBar}></View>
        </View>
    );
}

const styles = StyleSheet.create({
    imageCard: {
        height: 300,
        width: '100%'
    },
    imageStyle: {
        height: '100%',
        width: '100%'
    },
    CardTextualInfo: {
        height: 60,
        width: '100%',
        backgroundColor: 'rgba(0,0,0,0.5)',
        position: 'absolute',
        paddingLeft: 18,
        bottom: 0,
        justifyContent: 'center'
    },
    CardTitle: {
        fontWeight: '500',
        color: '#fff',
        fontSize: 22
    },
    CardDescription: {
        fontWeight: '200',
        color: '#fff',
        fontSize: 16,
        alignContent: 'space-between'
    },
    InfoCategoryList: {
        paddingTop: 10,
        paddingBottom: 0,
        paddingLeft: 18,
        paddingRight: 60,
        width: '100%',
        flexDirection: 'row'
    },
    InfoCategoryListText: {
        textAlign: 'left',
        fontSize: 14 * 1.2,
        letterSpacing: 0.2
    },
    InfoEndHorizontalCrossBar: {
        borderBottomWidth: 0.6,
        borderBottomColor: '#c0c0c0',
        position: 'absolute',
        bottom: 0,
        left: 0,
        right: 0,
        zIndex: 20,
        marginTop: 5
    },
    foodDescHeaderTitlePanel: {
        backgroundColor: '#F5A623',
        width: 30 * 1.2,
        height: 30 * 1.2,
        marginRight: 40,
        borderRadius: 5,
        justifyContent: 'center',
        alignSelf: 'center'
    },
    foodDescHeaderTitle: {
        fontSize: 18,
        fontWeight: '500',
        color: '#ffffff',
        textAlignVertical: 'center',
        textAlign: 'center'
    }
});