import React, { Component } from 'react'
import { Text, View, Image, Animated, StyleSheet } from 'react-native'
import { } from '../components';
import { connect } from 'react-redux';
import { TouchableOpacity, ScrollView } from 'react-native-gesture-handler';
import MapView, { PROVIDER_GOOGLE, PROVIDER_DEFAULT, Marker } from 'react-native-maps';
import { requestLocationPermission } from '../../requestPermissions';
import { ImageCard, InfoHeader, CategoryList, HeaderTitle } from './foodDescComponent';
import styles from './FoodDescriptionStyles';
class FoodDescription extends Component {
    static navigationOptions = {
        headerTransparent: true,
        headerTintColor: 'white',
        headerStyle: {
            backgroundColor: 'rgba(0,0,0,0.0)',
            zIndex: 100,
            elevation: 0,
            shadowOpacity: 0,
            color: '#fff',
            borderBottomWidth: 0,
        },
        headerRight: <HeaderTitle />
    }

    componentWillMount() {
        requestLocationPermission();
    }
    render() {
        return (
            <ScrollView
                style={styles.scrollStyle}>
                <ImageCard
                    title={this.props.data.title} description={this.props.data.description} imageURL={this.props.data.imageURL} />
                <View style={styles.InfoPanel}>
                    <View style={styles.InfoBasicPanel}>
                        <View><Text style={styles.InfoTitle}>Info</Text></View>
                        <InfoHeader title={'ADDRESS'} />
                        <View style={styles.InfoAddress}>
                            <Text style={styles.InfoAddressText}>
                                {this.props.data.address}
                            </Text>
                        </View>
                        <InfoHeader title={'HOURS'} />

                        <View style={{ paddingTop: 10, paddingBottom: 14, borderBottomWidth: 0.6, borderBottomColor: '#c0c0c0', width: 180, flexDirection: 'row' }}>
                            <Text style={{ textTransform: 'uppercase', fontSize: 14 * 1.2 }}>9:00 am </Text>
                            <Text style={{ fontSize: 14 * 1.2, marginLeft: 3, marginRight: 3 }}>to</Text>
                            <Text style={{ textTransform: 'uppercase', fontSize: 14 * 1.2 }}>10:00 pm </Text>
                        </View>
                        <InfoHeader title={'CATEGORY'} />
                    </View>
                    <CategoryList categories={this.props.data.categories} />
                    <View style={styles.InfoLocationPanel} pointerEvents="none">
                        <MapView
                            provider={PROVIDER_DEFAULT}
                            style={{ height: 250, width: '100%' }}
                            region={this.props.data.region}>
                            <Marker coordinate={this.props.data.coordinates}>
                                {/* <View style={styles.InfoMarkerComponent}> */}
                                <Image source={require('./../../../assets/direction/direction_icn.png')} resizeMode="contain" style={{ width: 50, height: 80 }} />
                                {/* </View> */}
                            </Marker>
                        </MapView>
                    </View>
                </View>
                <View style={styles.InfoReviewBtnPanel}>
                    <TouchableOpacity style={styles.InfoReviewBtn}>
                        <Text style={styles.InfoReviewBtnText}>Review</Text>
                    </TouchableOpacity>
                </View>

            </ScrollView >
        )
    }
}

function mapStateToProps(state) {
    return {
        data: state.foodInformation
    }
}

export default connect(mapStateToProps)(FoodDescription);