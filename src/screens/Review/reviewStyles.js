import React, { Component } from 'react';
import { StyleSheet, Dimensions } from 'react-native';



const styles = {
    rootReviewList: {
        height: 120,
        width: Dimensions.get('window').width,
        marginTop: 2,
        flex: 1,
        flexDirection: 'row',
        marginTop: 10,
        borderBottomColor: '#c0c0c0',
        borderBottomWidth: 1
    }
};
export default styles;