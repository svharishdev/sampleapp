import React from 'react';
import { StyleSheet, Dimensions,TouchableOpacity,Image,View,Text } from 'react-native';
export function headerTitle() {
    return (
        <Text style={styles.headerTitle}>Review</Text>
    );
}
export function headerRight() {
    return (
        <Text>{'<'}</Text>
    );
}
export function ReviewList(props) {
    return (
        <TouchableOpacity style={styles.listTouchableBtn} onPress={props.navigateTo}>
            <View style={styles.imageViewList}>
                <Image style={styles.imageView} source={props.image} />
            </View>
            <View style={styles.listPanel}>
                <Text style={styles.listPanelName}>{props.item.name}</Text>
                <Text style={styles.listPanelSubTitle}>{props.item.subTitle}</Text>
                <Text style={styles.listPanelDescription}>{props.item.description}</Text>
            </View>
        </TouchableOpacity>
    );
}
const styles = StyleSheet.create({
    headerTitle: {
        textAlign: 'center',
        color: '#ffffff',
        flexDirection: 'column',
        fontSize: 21,
        flex: 1,
        fontWeight: '400',
        height: '100%',
        justifyContent: 'center',
        textAlignVertical: 'center'
    },
    listTouchableBtn: {
        height: 120,
        width: Dimensions.get('window').width,
        marginTop: 2,
        flex: 1,
        flexDirection: 'row',
        marginTop: 10,
        borderBottomColor: '#c0c0c0',
        borderBottomWidth: 1
    },
    imageViewList: {
        flex: 0.25,
    },
    imageView: {
        width: 50,
        height: 50,
        backgroundColor: '#ee1',
        alignSelf: 'center',
        marginTop: 5,
        borderRadius: 3
    },
    listPanel: {
        flex: 0.75
    },
    listPanelName: {
        marginLeft: 10,
        fontSize: 20,
        marginTop: 3,
        fontWeight: '400'
    },
    listPanelSubTitle: {
        marginLeft: 10,
        fontStyle: 'normal',
        color: '#c0c0c0',
        fontWeight: '200'
    },
    listPanelDescription: {
        textAlign: 'justify',
        marginRight: 50,
        marginLeft: 10,
        fontSize: 15,
    }

});
