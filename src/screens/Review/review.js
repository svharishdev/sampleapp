import React, { Component } from 'react'
import { Text, View, FlatList, Dimensions, StatusBar } from 'react-native'
import { connect } from 'react-redux';
import { headerTitle, ReviewList } from './reviewComponents';

import styles from './reviewStyles';
class Review extends Component {
    static navigationOptions = {
        headerTitle: headerTitle,
        headerStyle: {
            backgroundColor: '#0080c0',
            color: '#fff'
        },
    };

    openFoodDescription = () => {
        console.log("Opening Food Description");
        this.props.navigation.navigate('food');
    }

    render() {
        let profile = require('./../../../assets/pragya.png');
        return (
            <View style={{ flex: 1, marginTop: 10, }}>
                <FlatList
                    data={this.props.dataStore}
                    renderItem={({ item }) =>
                        <ReviewList item={item} image={profile} navigateTo={this.openFoodDescription} styles={styles} />
                    }
                    keyExtractor={(item, index) => index.toString()}
                />
            </View>
        )
    }
}

function mapStateToProps(state) {
    return {
        dataStore: state.dataStore
    };
};

function mapDispatchToProps(dispatch) {
    return {
        onIncrementCounter: () => dispatch({ type: 'INCREMENT' })
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Review);