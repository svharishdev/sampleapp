import React, { Component } from 'react'
import { Text, View, Animated, Image, Dimensions, StyleSheet, TextInput } from 'react-native'
import MapView, { Marker, PROVIDER_GOOGLE } from 'react-native-maps';
import { connect } from 'react-redux';

import * as actionTypes from '../../redux/actions';


const { width, height } = Dimensions.get("window");

const HEIGHT = height / 8;
const WIDTH = width - 10;

class LocationToSearch extends Component {
    static navigationOptions = {
        header: null
    }
    state = {
        search: '',
        update: false,
        markers: [
            {
                location: {
                    latitude: 12.9023221,
                    longitude: 77.6014726,
                },
                name: "Planet Cafe",
                address: "Manipal, Karnataka",
                rating: 3.1,
                photo: "",
                category: 'Pizza'
            }, {
                location: {
                    latitude: 12.8023221,
                    longitude: 75.601472,
                },
                name: "Planet Cafe",
                address: "Manipal, Karnataka",
                rating: 3.1,
                photo: "",
                category: 'Pizza'
            }
        ],
        region: {
            latitude: 12.9023221,
            longitude: 77.6014726,
            latitudeDelta: 0.041,
            longitudeDelta: 0.2
        },
        places: []
    }
    constructor(props) {
        super(props);
        console.log("API Places",props.ApiPlaces);
        this.setState({ markers: props.ApiPlaces });

    }
    componentWillMount() {
        this.index = 0;
        this.animation = new Animated.Value(0);
        this.indexAnim = new Animated.Value(0);
    }
    componentWillReceiveProps(nextProps) {
        console.log("Next Props", nextProps);
        this.updateStatewithPlaces(nextProps.places);
    }
    updateStatewithPlaces = (places) => {
        alert("Updated");
        console.log(this.state.markers);
        let place = this.state.markers.concat(places);
        this.setState({ markers: place });
        this.forceUpdate();
    }
    componentDidMount() {
        console.log("PLACES :::: ", this.state.markers);

        this.animation.addListener(({ value }) => {
            let index = Math.floor(value / WIDTH + 0.8);
            if (index >= this.state.markers.length) {
                index = this.state.markers.length - 1;
            }
            if (index <= 0) {
                index = 0;
            }

            clearTimeout(this.regionTimeout);
            this.regionTimeout = setTimeout(() => {
                if (this.index !== index) {
                    this.index = index;
                    console.log(index, "INDEX VALUE SCROLLED NOW");
                    this.indexAnim.setValue(index);
                    const { coordinate } = this.state.markers[index];
                    this.map.animateToRegion(
                        {
                            ...coordinate,
                            latitudeDelta: this.state.region.latitudeDelta,
                            longitudeDelta: this.state.region.longitudeDelta,
                        },
                        350
                    );
                }
            }, 10);
        });
    }
    componentWillUnmount() {
        this.animation.removeAllListeners();
    }
    render() {

        const interpolations = this.state.markers.map((marker, index) => {
            const inputRange = [
                (index - 1) * WIDTH,
                index * WIDTH,
                ((index + 1) * WIDTH),
            ];
            const color = this.indexAnim.interpolate({
                inputRange: [0, 1],
                outputRange: ['rgba(112,111,10,1)', 'rgba(255,0,0,1)'],
            });
            console.log("[INDEX] ::: ", inputRange);

            return {};
        });

        return (
            <View style={styles.container}>
                <View style={{ width: '96%', height: 51, position: 'absolute', top: 0, margin: 10, zIndex: 100, backgroundColor: '#fff' }}>
                    <TextInput placeholder={'Enter Location...'} style={{ alignContent: 'flex-end' }}
                        onSubmitEditing={() => this.props.searchPlaceInApi(this.state.search, [this.state.region.latitude, this.state.region.longitude])}
                        onChangeText={(text) => this.setState({ search: text })}
                        returnKeyType={'search'}
                        keyboardType={'web-search'} />
                </View>
                <MapView
                    ref={map => this.map = map}
                    initialRegion={this.state.region}
                    showsUserLocation={true}
                    style={styles.container}
                >
                    {this.state.markers.map((marker, index) => {
                        const bgStyle = {
                            backgroundColor: 'rgba(112,110,0,1)',
                        }
                        return (
                            <MapView.Marker key={index} coordinate={marker.location}>
                                <Animated.View style={[styles.markerWrap]}>
                                    <Animated.View style={[styles.ring]} />
                                    <View style={[styles.marker, { backgroundColor: bgStyle }]}>
                                        <Text style={styles.markerText}>{index + 1}</Text>
                                    </View>
                                </Animated.View>
                            </MapView.Marker>
                        );
                    })}
                </MapView>
                <Animated.ScrollView
                    horizontal
                    scrollEventThrottle={1}
                    showsHorizontalScrollIndicator={false}
                    snapToInterval={WIDTH}
                    onScroll={Animated.event(
                        [
                            {
                                nativeEvent: {
                                    contentOffset: {
                                        x: this.animation,
                                    },
                                },
                            },
                        ],
                        { useNativeDriver: true }
                    )}
                    style={styles.scrollView}
                    contentContainerStyle={styles.endPadding}>
                    {this.state.markers.map((marker, index) => (

                        <View style={styles.card} key={index}>
                            <Image
                                // source={marker.image}
                                // source={marker.photo}
                                style={styles.cardImage}
                                resizeMode="cover"
                            />
                            <View style={styles.textContent}>
                                <Text numberOfLines={1} style={{ fontSize: 21, color: '#0076FF', fontWeight: '300' }}>{marker.name}</Text>
                                <View style={{ position: 'absolute', bottom: 0 }}>
                                    <Text numberOfLines={1} style={{ fontSize: 18, fontWeight: '200' }}>{marker.category}</Text>
                                    <View style={{ alignContent: 'flex-end', flexDirection: 'row' }}>
                                        <Text numberOfLines={1} style={{ fontSize: 14, marginBottom: 5 }}>{'0'}</Text>
                                        <Text numberOfLines={1} style={{ fontSize: 14, marginBottom: 5 }}>{marker.address}</Text>
                                    </View>
                                </View>

                            </View>
                        </View>
                    ))}
                </Animated.ScrollView>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    scrollView: {
        position: "absolute",
        bottom: 10,
        left: 0,
        right: 0,
        paddingVertical: 10,
    },
    endPadding: {
        paddingRight: width - WIDTH,
    },
    card: {
        padding: 10,
        flexDirection: 'row',
        elevation: 2,
        backgroundColor: "#FFF",
        marginHorizontal: 10,
        shadowColor: "#000",
        shadowRadius: 5,
        shadowOpacity: 0.3,
        shadowOffset: { x: 2, y: -2 },
        height: HEIGHT,
        width: WIDTH,
        overflow: "hidden",
    },
    cardImage: {
        flex: 1,
        width: "100%",
        height: "100%",
        alignSelf: "center",
        backgroundColor: '#ee1'
    },
    textContent: {
        flex: 3,
        marginLeft: 10
    },
    cardtitle: {
        fontSize: 12,
        marginTop: 5,
        fontWeight: "bold",
    },

    markerWrap: {
        alignItems: "center",
        justifyContent: "center",
    },
    markerText: {
        justifyContent: 'center',
        alignSelf: 'center',
        color: '#fff'
    },
    marker: {
        width: 25,
        height: 25,
        borderRadius: 25,
        backgroundColor: "#C4DE36",
        justifyContent: 'center',
        alignSelf: 'center'
    },
    ring: {
        width: 24,
        height: 24,
        borderRadius: 12,
        backgroundColor: "rgba(170,240,10, 0.8)",
        position: "absolute",
        borderWidth: 1,
        borderColor: "rgba(170,240,10, 0.5)",
    },
});
function mapStateToProps(state) {
    return {
        places: state.mapData.ApiPlaces
    }
}
function mapDispatchToProps(dispatch) {
    return {
        searchPlaceInApi: (placeName, coorids = "10.652814,-61.3969835") => dispatch(actionTypes.fetchRecords(placeName, coorids.toString()))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(LocationToSearch);