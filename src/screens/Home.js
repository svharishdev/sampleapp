import React, { Component } from 'react';
import { View, Text, TouchableOpacity, Image } from 'react-native';

export default class Home extends Component {
    static navigationOptions = {
        headerTitle: <View style={{ flex: 1, width: '100%', height: '100%', justifyContent: 'center', alignSelf: 'center' }}>
            <Text style={{ textAlign: 'center', fontWeight: 'bold', fontSize: 20 }}>Cafe Sample Project</Text>
        </View>
    }
    constructor(props) {
        super(props);
        this.state = {
        };
    }

    render() {
        return (
            <View style={{ flex: 1, justifyContent: 'center', alignSelf: 'center' }}>
                <TouchableOpacity style={{ height: 51, width: 250, backgroundColor: '#F5A623', borderRadius: 6 }} onPress={() => this.props.navigation.navigate('review')}>
                    <View style={{ flex: 1, justifyContent: 'center', alignSelf: 'center', textAlignVertical: 'center' }}>
                        <Text style={{ color: '#fff', fontSize: 18 }}>Get Started</Text>
                    </View>
                </TouchableOpacity>
            </View>
        );
    }
}
