import React, { Component } from 'react'
import { Text, View, TextInput, ScrollView, Animated, Dimensions, StyleSheet, FlatList, Image } from 'react-native'
import MapView, { Marker, PROVIDER_GOOGLE } from 'react-native-maps';
import { connect } from 'react-redux';
import Geolocation from '@react-native-community/geolocation';
import * as actionTypes from '../../redux/actions';
import { HeaderTitle } from './../FoodDescription/foodDescComponent';
import Icon from 'react-native-vector-icons/dist/FontAwesome';
import { SearchBar } from './locationComponents';
import { TouchableOpacity } from 'react-native-gesture-handler';

const { width, height } = Dimensions.get('window');
const HEIGHT = height / 8;
const WIDTH = width - 20;


class LocationToSearch extends Component {
    static navigationOptions = {
        header: null
    }
    constructor(props) {
        super(props);

        this.state = {
            search: '',
            location: {
                latitude: 12.7023221,
                longitude: 77.6014726,
                latitudeDelta: 0.041,
                longitudeDelta: 0.2
            },
            places: []
        }
    }
    setLocation = (lat, lng) => {
        let loc = {
            latitude: lat,
            longitude: lng,
            longitudeDelta: this.state.location.longitudeDelta,
            latitudeDelta: this.state.location.latitudeDelta
        }
        this.setState({ location: loc });
    }
    async componentDidMount() {

        await Geolocation.getCurrentPosition(info => this.setLocation(info.coords.latitude, info.coords.longitude));
        console.log("UPdated Location ::: ", this.state.location);
    }
    render() {
        const defaultPhoto = require('./../../../assets/default.jpg');
        return (
            <View style={{ flex: 1 }}>
                <SearchBar submitState={(text) => this.setState({ search: text })} searchAction={this.props.searchPlaceInApi} coords={[this.state.location.latitude, this.state.location.longitude]} search={this.state.search} />
                <MapView
                    style={{ flex: 1 }}
                    initialRegion={this.state.location}
                    showsCompass={true}
                    showsMyLocationButton={true}
                    showsUserLocation={true}
                    provider={PROVIDER_GOOGLE}>
                    {this.props.places.map((marker, index) => {
                        console.log(marker, index);
                        const bgStyle = {
                            backgroundColor: 'rgba(112,110,0,1)',
                        }
                        return (
                            <MapView.Marker key={index} coordinate={marker.location}>
                                <Animated.View style={[styles.markerWrap]}>
                                    <Animated.View style={[styles.ring]} />
                                    <View style={[styles.marker, { backgroundColor: bgStyle }]}>
                                        <Text style={styles.markerText}>{index + 1}</Text>
                                    </View>
                                </Animated.View>
                            </MapView.Marker>
                        );
                    })}
                </MapView>
                <FlatList
                    horizontal
                    showsHorizontalScrollIndicator={false}
                    data={this.props.places}
                    style={styles.scrollView}
                    keyExtractor={(item, index) => index.toString()}
                    renderItem={(marker, index) => (
                        <TouchableOpacity style={styles.card} key={index} onPress={() => { console.log(index) }}>
                            <View style={styles.foodDescHeaderTitlePanel}>
                                <Text style={styles.foodDescHeaderTitle}>8.5</Text>
                            </View>
                            <Image
                                // source={marker.image}
                                source={marker.item.photo == null ? defaultPhoto : { uri: marker.item.photo }}
                                style={styles.cardImage}
                                resizeMode="cover"
                            />
                            <View style={styles.textContent}>
                                <Text numberOfLines={1} style={styles.cardName}>{marker.item.name}</Text>
                                <View style={styles.cardPanel}>
                                    <Text numberOfLines={1} style={styles.category}>{marker.item.category}</Text>
                                    <View style={styles.categorydetails}>
                                        <Text numberOfLines={1} style={styles.distance}>{'0'}</Text>
                                        <Text numberOfLines={1} style={styles.address}>{marker.item.address}</Text>
                                    </View>
                                </View>
                            </View>
                        </TouchableOpacity>
                    )}
                />
            </View>
        )
    }
}

const styles = StyleSheet.create({
    cardName: {
        fontSize: 21,
        color: '#0076FF',
        fontWeight: '300'
    },
    distance: {
        fontSize: 14, marginBottom: 5, paddingRight: 10
    },
    address: {
        fontSize: 14, marginBottom: 5
    },
    foodDescHeaderTitlePanel: {
        backgroundColor: '#F5A623',
        position: 'absolute',
        right: 1,
        top: 10,
        zIndex: 100,
        width: 30 * 1.2,
        height: 30 * 1.2,
        marginRight: 40,
        borderRadius: 5,
        justifyContent: 'center',
        alignSelf: 'center'
    },
    foodDescHeaderTitle: {
        fontSize: 18,
        fontWeight: '500',
        color: '#ffffff',
        textAlignVertical: 'center',
        textAlign: 'center'
    },
    cardPanel: {
        position: 'absolute', bottom: 0
    },
    category: {
        fontSize: 18, fontWeight: '200'
    },
    categorydetails: {
        alignContent: 'flex-end', flexDirection: 'row'
    },
    markerText: {
        justifyContent: 'center',
        alignSelf: 'center',
        color: '#fff'
    },
    scrollView: {
        position: "absolute",
        bottom: 10,
        left: 0,
        right: 0,
        paddingVertical: 10,
    },
    endPadding: {
        paddingRight: width - WIDTH,
    },
    card: {
        padding: 10,
        flexDirection: 'row',
        elevation: 2,
        backgroundColor: "#FFF",
        marginHorizontal: 10,
        shadowColor: "#000",
        shadowRadius: 5,
        shadowOpacity: 0.3,
        shadowOffset: { x: 2, y: -2 },
        height: HEIGHT,
        width: WIDTH,
        overflow: "hidden",
    },
    cardImage: {
        flex: 1,
        width: "100%",
        height: "100%",
        alignSelf: "center",
        backgroundColor: '#ee1'
    },
    textContent: {
        flex: 3,
        marginLeft: 10
    },
    cardtitle: {
        fontSize: 12,
        marginTop: 5,
        fontWeight: "bold",
    },
    marker: {
        width: 25,
        height: 25,
        borderRadius: 25,
        backgroundColor: "#C4DE36",
        justifyContent: 'center',
        alignSelf: 'center'
    },
    markerWrap: {
        alignItems: "center",
        justifyContent: "center",
    },
    ring: {
        width: 24,
        height: 24,
        borderRadius: 12,
        backgroundColor: "rgba(170,240,10, 0.8)",
        position: "absolute",
        borderWidth: 1,
        borderColor: "rgba(170,240,10, 0.5)",
    },
});

function mapStateToProps(state) {
    return {
        places: state.mapData.ApiPlaces
    }
}
function mapDispatchToProps(dispatch) {
    return {
        searchPlaceInApi: (placeName, coorids = "10.652814,-61.3969835") => dispatch(actionTypes.fetchRecords(placeName, coorids.toString()))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(LocationToSearch);