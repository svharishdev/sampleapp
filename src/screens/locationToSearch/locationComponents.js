import React from 'react';
import {View,Text,TextInput,StyleSheet} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';

export function SearchBar(props) {
    return (
        <View style={styles.searchPanel}>
            <Icon name="search" size={25} name="search" style={styles.iconStyle} />
            <TextInput placeholder={'Enter Location...'} style={styles.textStyle}
                onSubmitEditing={() => props.searchAction(props.search, props.coords)}
                onChangeText={(text) => { props.submitState(text) }}
                returnKeyType={'search'}
                keyboardType={'web-search'} />
        </View>
    );
}

const styles = StyleSheet.create({
    searchPanel: {
        width: '96%',
        height: 51,
        position: 'absolute',
        top: 0,
        margin: 10,
        zIndex: 100,
        backgroundColor: '#fff',
        flexDirection: 'row',
        alignContent: 'space-between'
    },
    iconStyle: { padding: 10, },
    textStyle: { flex: 1, }
});