
var foursquare = require('react-native-foursquare-api')({
    clientID: 'WTSTI4VIYDJIMZPAQX1LFLRWBM5Q32K3EITFAEBN02MT1JAZ',
    clientSecret: 'EPU4Z0LJPEDZNYHZJKPYXF3I5CPNA1ZW15KTLHFJLXEIDMYK',
    style: 'foursquare', // default: 'foursquare'
    version: '20190806' //  default: '20140806'
});


export const FETCH_RECORDS = 'FETCH_RECORDS';
formatObject = (venueId) => {
    return {
        name: venueId.name,
        category: venueId.categories[0]["name"],
        rating: venueId.rating !== undefined ? venueId.rating : 0.0,
        photo: formatPhoto(venueId.bestPhoto),
        address: formatAddress(venueId.location.formattedAddress),
        location: this.formatLocation(venueId.location.lat, venueId.location.lng)
    }
}

formatLocation = (lat, lng) => {
    return {
        latitude: lat,
        longitude: lng
    }
}
formatPhoto = (photo = 'NONE') => {
    return photo == 'NONE' ? null : photo.prefix + photo.width + "x" + photo.height + photo.suffix;
}
formatAddress = (addr = ["-", "-"]) => {
    let addrStr = "";
    addr.forEach(function (str, index) {
        addrStr += str + ((addr.length - 1) == index ? " " : ", ")
    });
    return addrStr;
}

export const fetchRecords = (searchQuery = "Manipal", coordinates) => {

    return dispatch => {

        let venueList = [];
        let params = {
            "ll": coordinates,
            "query": searchQuery,
            "limit": 20
        };
        dispatch(fetchRecordsPending());
        foursquare.venues.getVenues(params)
            .then(function (venues) {
                console.log("Venues are :::: ", venues.response.venues);
                size = venues.response.venues.length;
                console.log("Total venues :::: ", size);
                venues.response.venues.forEach(async function (venue, index) {
                    console.log("[VENUES]", venue.id);
                    await foursquare.venues.getVenue(venue.id)
                        .then((item) => {
                            console.log("Details Venue", item);
                            venueList.push(formatObject(item.response.venue));
                            console.log("ForEach Index", index);
                            // return item.response.venue;
                        }).catch((error) => {
                            console.log(error);
                        });
                    // eleList += 1;
                    if (index+1 === size) {
                        dispatch(fetchRecordsSuccess(venueList));
                    }
                });
            })
            .catch(function (err) {
                console.log(err);
                dispatch(fetchRecordsFailure(err));
            });
        // dispatch(fetchRecordsSuccess(getFourSquareData(data)));
    }
}

export const FETCH_RECORDS_PENDING = 'FETCH_RECORDS_PENDING';

export const fetchRecordsPending = () => {
    return {
        type: FETCH_RECORDS_PENDING,
        status: 1
    }
}

export const FETCH_RECORDS_SUCCESS = 'FETCH_RECORDS_SUCCESS';

export const fetchRecordsSuccess = (data) => {
    return {
        type: FETCH_RECORDS_SUCCESS,
        ApiPlaces: data,
        status: 2
    }
}

export const FETCH_RECORDS_FAILURE = 'FETCH_RECORDS_FAILURE';

export const fetchRecordsFailure = (error) => {
    return {
        type: FETCH_RECORDS_FAILURE,
        status: 3,
        error: error
    }
}