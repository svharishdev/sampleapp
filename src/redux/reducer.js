import * as actionTypes from './actions';
const initialStore = {
    dataStore: [
        { name: 'Pragya K', subTitle: '10 Nov 2018', description: 'He is a React native developer with many possibilities', profile: './../../../assets/pragya.png' },
        { name: 'Adithya', subTitle: '10 Nov 2018', description: 'Chicken Club sandwich is highly recommended.', profile: './../../../assets/pragya.png' },
        { name: 'Pragya K', subTitle: '10 Nov 2018', description: 'Oreo Shake!!!', profile: './../../../assets/pragya.png' },
        { name: 'Adithya', subTitle: '10 Nov 2018', description: 'He is a React native developer with many possibilities', profile: './../../../assets/pragya.png' }
    ],
    foodInformation: {
        title: "Planet Cafe",
        description: "Sandwiches",
        imageURL: "https://veeba.in/wp-content/uploads/2017/11/Chilli-Rainbow-Sandwich-1-1024x535.jpg",
        address: "Near Mandavi Paradise, \n Manipal 576107, \n Karnataka",
        categories: ['Sandwitch Place', 'Burger Joint', 'Burrito Place'],
        region: {
            latitude: 13.3553235,
            longitude: 74.7805702,
            latitudeDelta: 0.0099,
            longitudeDelta: 0.001,
        },
        coordinates: { latitude: 13.3553235, longitude: 74.7805702 }
    },
    mapData: {
        placeName: '',
        ApiPlaces: []
    }
}

const reducer = (state = initialStore, action) => {
    switch (action.type) {
        case actionTypes.SEARCH_PLACE:
            return {
                ...state,
                mapData: {
                    ...state.mapData,
                    placeName: action.place
                }
            }
        case actionTypes.FETCH_RECORDS_SUCCESS:
            return {
                ...state,
                mapData: {
                    ...state.mapData,
                    ApiPlaces: action.ApiPlaces,
                    status: action.status
                }
            }
        case actionTypes.FETCH_RECORDS_PENDING:
            return {
                ...state,
                mapData: {
                    ...state.mapData,
                    status: action.status
                }
            }
        case actionTypes.FETCH_RECORDS_FAILURE:
            return {
                ...state,
                mapData: {
                    ...state.mapData,
                    status: action.status
                }
            }
    }
    return state;
}

export default reducer;