
import React, { Component } from 'react';
import { createStackNavigator, createAppContainer } from 'react-navigation';
import Review from './src/screens/Review/review';
import FoodDescription from './src/screens/FoodDescription/foodDescription';
import { Provider } from 'react-redux';
import reducer from './src/redux/reducer';
import requestLocationPermission from './src/requestPermissions';
import { createStore, applyMiddleware } from 'redux';
import Permissions from 'react-native-permissions';
import Home from './src/screens/Home';
import LocationToSearch from './src/screens/locationToSearch/location';
import LocationCard from './src/screens/locationCards/locationCards';
import logger from 'redux-logger';
import thunk from 'redux-thunk';
const Stack = createStackNavigator({
  location: { screen: LocationToSearch },
  locationCard: { screen: LocationCard },
  Home: { screen: Home },
  food: { screen: FoodDescription },
  review: { screen: Review }
},
  { initialRouteName: 'Home' });

const Navigation = createAppContainer(Stack);

const store = createStore(reducer, applyMiddleware(logger, thunk));
class App extends Component {
  constructor(props) {
    super(props)

    this.state = {
      locationPermission: 'undetermined'
    };
  };

  async componentDidMount() {
    // await requestLocationPermission();
    await this._checkPermissionStatusOf('location');
    console.log("DidMount", this.state.locationPermission);
    this.checkAndReqeustPermission(this.state.locationPermission);
  }
  checkAndReqeustPermission = (getStatus, type) => {
    switch (getStatus) {
      case 'authorized':
        break;
      case 'denied':
      case 'restricted':
      case 'undetermined':
        this.requestPermission(type);
        break;
    }
  }
  _checkPermissionStatusOf(type) {
    Permissions.check(type)
      .then((getStatus) => {
        console.log(getStatus);
        this.checkAndReqeustPermission(getStatus, type);
      }).catch((error) => {
        console.log("Error Reached", error);
      });
  }
  requestPermission = (type) => {
    Permissions.request(type)
      .then((getStatus) => {
        console.log("Requst Updated", getStatus);
        this.setState({ locationPermission: getStatus });
      }).catch((err) => {
        console.log("Error Found ::: ", err);
      })
  }
  render() {
    return (
      <Provider store={store}>
        <Navigation />
      </Provider>
    )
  };
}
export default App;